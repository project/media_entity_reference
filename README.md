CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Usage
 * Configuration

INTRODUCTION
------------

This module will allow the creation of new media types using the media
source plugin called 'Media Reference'.

This new media source allows the creation of medias referencing other media
entities.

REQUIREMENTS
------------

No special requirements.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/docs/extending-drupal/installing-modules
   for further information.

USAGE
------------

1. Download and enable as like any other module.
2. You can now create a new media type using the Media Reference source.
3. You can now create new media referencing existing ones, you can add custom
   fields to override alt or define custom captions, for example.

CONFIGURATION
-------------

No configuration is needed.
