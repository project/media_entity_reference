<?php

namespace Drupal\media_entity_reference\Plugin\media\Source;

use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\media\MediaInterface;
use Drupal\media\MediaSourceBase;
use Drupal\media\MediaTypeInterface;

/**
 * Media source wrapping around media reference entity fields.
 *
 * @see \Drupal\file\FileInterface
 *
 * @MediaSource(
 *   id = "reference",
 *   label = @Translation("Reference"),
 *   description = @Translation("Reference another entity from a media."),
 *   allowed_field_types = {"entity_reference"},
 *   default_thumbnail_filename = "no-thumbnail.png",
 *   forms = {
 *     "media_library_add" = "\Drupal\media_entity_reference\Form\ReferenceMediaLibraryAddForm",
 *   }
 * )
 */
class MediaEntityReference extends MediaSourceBase {

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function createSourceField(MediaTypeInterface $type) {
    return parent::createSourceField($type)->set('label', 'Reference');
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {

    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $media->get($this->configuration['source_field'])->entity;

    // If the source field is not required, it may be empty.
    if (!$entity) {
      return parent::getMetadata($media, $attribute_name);
    }

    if ($entity instanceof MediaInterface) {
      return $entity->getSource()->getMetadata($entity, $attribute_name);
    }

    return parent::getMetadata($media, $attribute_name);
  }

  /**
   * {@inheritdoc}
   */
  public function prepareViewDisplay(MediaTypeInterface $type, EntityViewDisplayInterface $display) {
    $display->setComponent($this->getSourceFieldDefinition($type)->getName(), [
      'type' => 'entity_reference_entity_view',
      'label' => 'hidden',
    ]);
  }

}
